DIGISPARK ATTINY85 Repo
-------

The `DigiSpark_HID_Attack_Mac` and `DigiSpark_HID_Attack_Windows-Linux` directories hold .ino files for some automated keystroke command injection/HID Attack.

I bought me a handful of [these bad boys](https://www.aliexpress.com/item/1pcs-Digispark-kickstarter-development-board-ATTINY85-module-for-Arduino-usb/32584084654.html).

## PC / Board Setup:

First download [the Arduino IDE](https://www.arduino.cc/en/main/software)

Download [DigiSpark drivers](https://github.com/digistump/DigistumpArduino/releases). Install the right arch - 32bit or 64bit based on *your system*.

Step 2
Open Arduino IDE:

- Add additional board manager url: Files>Preferences> 
- in the additional board manager field: http://digistump.com/package_digistump_index.json and click OK.

- Now go to: Tools>board>board manager 

- change the “type” to Contributed
- select the Digistump AVR Boards by Digistump version 1.6 
- click on install, the package will be downloaded and installed.
- After installation goto Tools>Board> select Digispark (Default 16.5Mhz)
- Change programmer type Tools>Programmer> select USBtinyISP 

All set to program the board!