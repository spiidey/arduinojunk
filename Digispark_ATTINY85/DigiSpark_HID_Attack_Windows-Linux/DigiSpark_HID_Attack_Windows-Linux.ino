#include "DigiKeyboard.h"

#define KEY_DELETE 42
#define KEY_TAB 43
#define KEY_CAPSLOCK 57
#define KEY_PRINTSCREEN 70
#define KEY_SCROLLLOCK 71
#define KEY_PAUSE 72
#define KEY_INSERT 73
#define KEY_HOME 74
#define KEY_PAGEUP 75
#define KEY_END 77
#define KEY_PAGEDOWN 78
#define KEY_RIGHT 79
#define KEY_RIGHTARROW 79
#define KEY_LEFT 80
#define KEY_LEFTARROW 80
#define KEY_DOWN 81
#define KEY_DOWNARROW 81
#define KEY_UP 82
#define KEY_UPARROW 82
#define KEY_NUMLOCK 83
#define KEY_MENU 118

void setup() {
  pinMode(1, OUTPUT);
  pinMode(0, OUTPUT);
  digitalWrite(0, LOW);
  digitalWrite(1, LOW); // Make damn sure the LEDs are turned off
  DigiKeyboard.sendKeyStroke(KEY_R,MOD_GUI_RIGHT); // Super-key + R (Run Prompt)
  DigiKeyboard.delay(2000); // Tight for long URL...
  DigiKeyboard.sendKeyStroke(0);
  DigiKeyboard.println("https://gitlab.com/spiidey/");
  DigiKeyboard.delay(1500);
  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.sendKeyStroke(0);
}


void loop() {
  // Blink the RED LED to let us know the command's completed:
  digitalWrite(0, HIGH); //Turn LED ON
  digitalWrite(1, HIGH);

  
  delay(1000);
  digitalWrite(0, LOW); //Turn LED OFF
  digitalWrite(1, LOW);
  delay(1000);
}

/**/
